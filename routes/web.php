<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\MenuController;
use App\Models\Transaksi;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});
Route::get('/menu', [MenuController::class, 'index']);
Route::get('/menu/{id}', [MenuController::class, 'show']);
Route::post('/menu', [MenuController::class, 'store']);
Route::delete('/menu/{id}', [MenuController::class, 'destroy']);
Route::get('/transaksi/update', [TransaksiController::class, 'update']);
Route::get('/produk/pdf', [ProductController::class, 'exportPDF']);
Auth::routes();
Route::get('/master', function () {
    return view('layout.master');
});
Route::resource('kategori', CategoryController::class);
Route::resource('produk', ProductController::class);
Route::resource('transaksi', TransaksiController::class);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
