<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $fillable=['nota','produk_id','qty','user_id','status'];
    
   public function produk()
    {
        return $this->belongsTo(Product::class,'produk_id');
    }
}
