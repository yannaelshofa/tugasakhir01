<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['nama_produk','harga','stok','kategori_id','foto'];
   // use HasFactory;
    public function kategori(){
     
        return $this->belongsTo(Category::class, 'kategori_id');
    
    }
}
