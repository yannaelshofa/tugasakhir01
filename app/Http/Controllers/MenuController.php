<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index(){
        $datas=Product::all();
        $datas2 = Transaksi::where('status', 1)->get();
        return view('menu.index',compact('datas','datas2'));
    }
    public function show($id){
        $datas=Product::find($id);
       
        return view('menu.detil',compact('datas'));
    }
    public function store(Request $request)
    {
       
        Transaksi::create([
            'produk_id'=>$request->produk_id,
            'qty'=>$request->qty,
            'user_id'=>1,
           
        ]);
        echo "sukses";
        return redirect('/menu')->with('success', 'Data Berhasil Ditambahkan');
    }
    public function destroy($id)
    {
        $post = Transaksi::find($id);
        $post->delete();
        return redirect('/menu')->with('success', 'Data Berhasil Dihapus');
    }

}
