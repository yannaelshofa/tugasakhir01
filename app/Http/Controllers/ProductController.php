<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
// use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function index()
    {
        $datas=Product::all();
       //dd($datas->ketegori);
         return view("produk.index",compact('datas'));
    }
    public function exportPDF() {
      
        $datas = Product::all();
        
        // $pdf = PDF::loadView('produk.index', ['datas' => $datas]);
        $pdf = PDF::loadView('produk.index2',compact('datas'));
        
        return $pdf->download('product.pdf');
        
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori=Category::all();
        return view("produk.create",compact('kategori'));
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_produk'=>'required'
        ]);
        // Product::create([
        //     'nama_produk'=>$request->nama_produk,
        //     'harga'=>$request->harga,
        //     'stok'=>$request->stok,
        //     'kategori_id'=>$request->kategori_id,
            
        // ]);
        $model = new Product();
        $model->nama_produk = $request->nama_produk;
        $model->harga = $request->harga;
        $model->stok = $request->stok;
        $model->kategori_id = $request->kategori_id;
        if ($request->file('foto')) {
            $file = $request->file('foto');
            $nama_file = time() . str_replace('', '', $file->getClientOriginalName());
            $file->move('foto', $nama_file);
            $model->foto = $nama_file;
        }
        $model->save();
       
        return redirect('/produk')->with('success', 'Data Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datas = Product::find($id);
        $kategori=Category::all();
        return view("produk.edit",compact("datas","kategori"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model =Product::find($id);
        $model->nama_produk=$request->nama_produk;
        $model->harga=$request->harga;
        $model->stok=$request->stok;
        $model->update();
        return redirect('/produk')->with('success', 'Data Berhasil Diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $post = Product::find($id);
        $post->delete();
        return redirect('/produk')->with('success', 'Data Berhasil Dihaspus!');
    }
}
