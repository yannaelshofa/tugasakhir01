<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('template/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            {{-- <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div> --}}
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
   with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/kategori" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Kategori
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>

            </li>
            {{-- <li class="nav-item">
                <a href="../widgets.html" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Tabel
                        <span class="right badge badge-danger">New</span>
                    </p>
                </a> --}}
            {{-- <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/tabel" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Tabel</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="data-tabel" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Tabel v2</p>
                        </a>
                    </li>

                </ul> --}}
            </li>
            <li class="nav-item">
                <a href="/produk" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Product

                    </p>
                </a>

            </li>
            <li class="nav-item">
                <a href="/transaksi" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Kasir

                    </p>
                </a>

            </li>
            {{-- <li class="nav-item">
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <input type="submit" value="Logout">

                </form>

            </li> --}}

        </ul>

    </nav>
    <!-- /.sidebar-menu -->
</div>
