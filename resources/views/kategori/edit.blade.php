@extends('layout.master')
@section('judul')
    Edit Data
@endsection
@section('content')
    <form action="/kategori/{{ $datas->id }}" method="POST">
        @csrf
        @method("PUT")
        <input type="hidden" name="id" value="{{ $datas->id }}">
        <div class="card-body">
            <div class="form-group">
                <label for="nama_kategori">Nama Kategori</label>
                <input type="text" class="form-control" id="" name="nama_kategori" value="{{ $datas->nama_kategori }}">
            </div>


            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
    </form>
@endsection
