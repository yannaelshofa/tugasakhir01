@extends('layout.master')
@section('judul')
    Data Kategori
@endsection
@push('script')
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush
@section('content')
    <a href="/kategori/create">Tambah Data</a>
    <table id="example1" class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>

                <th>Edit</th>
                <th>Hapus</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($datas as $item => $values)
                <tr>
                    <td>{{ $values->id }}</td>
                    <td>{{ $values->nama_kategori }}</td>

                    <td><a href="/kategori/{{ $values->id }}" class="btn btn-primary">Edit</a> </td>
                    <td>
                        <form action="/kategori/{{ $values->id }}" method="post">
                            @csrf
                            @method("DELETE")
                            <input type="submit" value="Hapus" class="btn btn-danger">
                        </form>
                    </td>


                </tr>
            @endforeach
        </tbody>
    </table>
    {{-- @include('sweetalert::alert') --}}
@endsection
