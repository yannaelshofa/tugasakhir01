@extends('layout.master')
@section('judul')
    Tambah Data
@endsection
@section('content')
    <form action="/kategori" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama_kategori">Nama Kategori</label>
                <input type="text" class="form-control" id="" name="nama_kategori" placeholder="Masukkan Kategori">
            </div>


            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
    </form>
@endsection
