<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Menu</title>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand">Menu</a>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <div class="row">
            @foreach ($datas as $item => $values)
                <div class="col-4 mb-4">
                    <div class="card shadow h-100" style="width: 18rem;">
                        {{-- <img src="{{ asset(.'/foto/'.values->image) }}" class="card-img-top" alt="..."> --}}
                        <img src="{{ asset('/foto/' . $values->foto) }}" alt="" class="card-img-top" title="">
                        <div class="card-body">
                            <h5 class="card-title"><strong>{{ $values->nama_produk }}</strong></h5>
                            <p class="card-text">Harga : {{ $values->harga }}</p>
                        </div>
                        <a href="menu/{{ $values->id }}" class="btn btn-success btn-sm">Detail</a>

                    </div>
                </div>
            @endforeach
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>QTY</th>
                    <th>Jumlah</th>
                    <th>Aksi</th>

                </tr>
            </thead>
            @php
                $i = 1;
                $total = 0;
            @endphp
            <tbody>
                @foreach ($datas2 as $item => $values)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $values->produk->nama_produk }}</td>
                        <td>{{ $values->produk->harga }}</td>
                        <td>{{ $values->qty }}</td>
                        <td>{{ $values->qty * $values->produk->harga }}</td>



                        <td>
                            <form action="/menu/{{ $values->id }}" method="post">
                                @csrf
                                @method("DELETE")
                                <input type="submit" value="Hapus">
                            </form>
                        </td>
                        @php
                            $total = $values->qty * $values->produk->harga + $total;
                        @endphp
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4">Total</td>
                    <td>{{ $total }}</td>
                </tr>
            </tbody>
        </table>

    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
    -->
    @include('sweetalert::alert')
</body>

</html>
