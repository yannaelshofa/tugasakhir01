@extends('layout.master')
@section('judul')
    Tambah Data
@endsection
@section('content')
    <form action="/produk" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama_produk">Nama produk</label>
                <input type="text" class="form-control" id="" name="nama_produk" placeholder="Masukkan produk">
            </div>
            <div class="form-group">
                <label for="nama_produk">Harga</label>
                <input type="text" class="form-control" id="" name="harga" placeholder="Harga">
            </div>
            <div class="form-group">
                <label for="nama_produk">Stok</label>
                <input type="text" class="form-control" id="" name="stok" placeholder="Stok">
            </div>
            <div class="form-group">
                <label for="foto">Foto</label>
                <input type="file" class="form-control" id="foto" name="foto" placeholder="Stok">
            </div>


            <div class="form-group">
                <label for="Kategori">Kategori</label>
                <select name="kategori_id" id="">
                    @foreach ($kategori as $item => $values)
                        <option value="{{ $values->id }}">{{ $values->nama_kategori }}</option>
                    @endforeach
                </select>
            </div>

            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
    </form>
@endsection
