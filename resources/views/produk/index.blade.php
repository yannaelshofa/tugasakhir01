@extends('layout.master')
@section('judul')
    Data Produk
@endsection
@push('script')
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush
@section('content')
    <a href="/produk/create" class="btn btn-warning">Tambah Data</a>
    <a href="produk/pdf" class="btn btn-primary">Expord pdf</a>

    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Stok</th>
                <th>Nama Kategori</th>
                <th>Edit</th>
                <th>Hapus</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($datas as $item => $values)
                <tr>
                    <td>{{ $item + 1 }}</td>
                    <td>{{ $values->nama_produk }}</td>
                    <td>{{ $values->harga }}</td>
                    <td>{{ $values->stok }}</td>
                    <td>{{ $values->kategori->nama_kategori }}</td>

                    <td>
                        <a href="/produk/{{ $values->id }}" class="btn btn-primary">Edit</a>

                    </td>
                    <td>
                        <form action="/produk/{{ $values->id }}" method="post">
                            @csrf
                            @method("DELETE")
                            <input type="submit" class="btn btn-danger" value="Hapus">
                        </form>
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
@stack('script')
