@extends('layout.master')
@section('judul')
    Tambah Data
@endsection
@section('content')
    <form action="/produk/{{ $datas->id }}" method="POST">
        @csrf
        @method("PUT")
        <div class="card-body">
            <div class="form-group">
                <label for="nama_produk">Nama produk</label>
                <input type="text" class="form-control" id="" name="nama_produk" value="{{ $datas->nama_produk }}">
            </div>
            <div class="form-group">
                <label for="nama_produk">Harga</label>
                <input type="text" class="form-control" id="" name="harga" value="{{ $datas->harga }}">
            </div>
            <div class="form-group">
                <label for="nama_produk">Stok</label>
                <input type="text" class="form-control" id="" name="stok" value="{{ $datas->stok }}">
            </div>

            <div class="form-group">
                <label for="Kategori">Kategori</label>
                <select name="kategori_id" id="">
                    @foreach ($kategori as $item => $values)
                        <option value="{{ $values->id }}">{{ $values->nama_kategori }}</option>
                    @endforeach
                </select>
            </div>

            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
    </form>
@endsection
