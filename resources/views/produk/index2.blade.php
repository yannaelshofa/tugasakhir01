<table id="yanna" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="width: 10px">#</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Stok</th>
            <th>Nama Kategori</th>


        </tr>
    </thead>
    <tbody>
        @foreach ($datas as $item => $values)
            <tr>
                <td>{{ $item + 1 }}</td>
                <td>{{ $values->nama_produk }}</td>
                <td>{{ $values->harga }}</td>
                <td>{{ $values->stok }}</td>
                <td>{{ $values->kategori->nama_kategori }}</td>


            </tr>
        @endforeach
    </tbody>
</table>
