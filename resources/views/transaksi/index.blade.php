@extends('layout.master')
@section('judul')
    Data Produk
@endsection
@section('content')
    {{-- <a href="/transaksi/create">Tambah Data</a> --}}
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>QTY</th>
                <th>Jumlah</th>
                <th>Aksi</th>

            </tr>
        </thead>
        @php
            $i = 1;
            $total = 0;
        @endphp
        <tbody>
            @foreach ($datas as $item => $values)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $values->produk->nama_produk }}</td>
                    <td>{{ $values->produk->harga }}</td>
                    <td>{{ $values->qty }}</td>
                    <td>{{ $values->qty * $values->produk->harga }}</td>



                    <td>
                        <form action="/produk/{{ $values->id }}" method="post">
                            @csrf
                            @method("DELETE")
                            <input type="submit" value="Hapus">
                        </form>
                    </td>
                    @php
                        $total = $values->qty * $values->produk->harga + $total;
                    @endphp
                </tr>
            @endforeach
            <tr>
                <td colspan="4">Total</td>
                <td>{{ $total }}</td>
            </tr>
        </tbody>
    </table>
    <a href="transaksi/update" class="btn btn-primary">Selesai</a>
@endsection
