@extends('layout.master')
@section('judul')
    Tambah Data
@endsection
@section('content')
    <form action="/transaksi" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama_produk">Nama produk</label>
                <input type="text" class="form-control" id="" name="produk_id" placeholder="id _ produk">
            </div>
            <div class="form-group">
                <label for="nama_produk">qty</label>
                <input type="text" class="form-control" id="" name="qty" placeholder="qty">
            </div>



            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
    </form>
@endsection
